import asyncio
from random import randint

from sanic import Sanic
from sanic.response import file

app = Sanic(__name__)


@app.route('/')
async def index(request):
    return await file('websocket.html')


@app.websocket('/feed')
async def feed(request, ws):
    while True:
        data = str(randint(0,100))
        print('Sending: ' + data)
        await ws.send(data)
        await asyncio.sleep(1)
        # data = await ws.recv()
        # print('Received: ' + data)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, debug=True)
